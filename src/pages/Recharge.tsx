import { IonButton,  IonInput, IonItem, IonLabel } from '@ionic/react'
import React, { useState } from 'react'
import '../assets/myappcss/Form.css'


function Recharge()  {
    var [valeur,setValeur] = useState("");
    var [message,setmessage] = useState("");
    var url = ""
    function save() {
        if (Number(valeur) < 0) {
            setmessage("valeur invalid !!!")
        }else{
            let demande = {
                "idUtilisateur" : sessionStorage.getItem("iduser"),
                "valeur" : ''+valeur,
                "etat" : ''+0
            }
            url = "http://encherebackend-production-21bf.up.railway.app/rechargerCompte";
            fetch(url,{
                method: "POST",
                headers: {"Content-type": "application/json" },
                body: JSON.stringify(demande)
            })
            setmessage("rechargement terminer")
            window.location.replace("/acceuil")
        }
    }

    return (
        <div className="ion-padding myform">
            <form>
                <IonLabel className='formtitle'>Rechargement</IonLabel>
                <br></br><br></br>
                <IonItem className='inputitem'>
                    <IonLabel position="floating">Valeur</IonLabel>
                    <IonInput onIonChange={(e: any)=> {setValeur(e.target.value)}} clearOnEdit={true} type="text"/>
                </IonItem>
                <br></br>
                <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={save}>
                    Recharger
                </IonButton>
                <p>{message}</p>
            </form>
        </div>
    )
}

export default Recharge;