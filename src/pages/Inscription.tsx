import { IonButton,  IonInput, IonItem, IonLabel } from '@ionic/react'
import React, { useState } from 'react'
import '../assets/myappcss/Form.css'


function Inscription()  {
    var [nom,setNom] = useState("");
    var [prenom,setPrenom] = useState("");
    var [datenaissance,setDateNaissance] = useState("");
    var [email,setEmail] = useState("");
    var [password,setPassword] = useState("");

    function save() {
        fetch("http://encherebackend-production-21bf.up.railway.app/saveUtilisateur/"+nom+"&"+prenom+"&"+datenaissance+"&"+email+"&"+password).then((response) => {
            return response.json();
        }).then((utilisateur)=>{
            sessionStorage.setItem("iduser",utilisateur.id)
            window.location.replace("/acceuil")
        });
    }

    return (
        <form className="ion-padding myform">
            <IonLabel className='formtitle'>Inscription</IonLabel>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Nom</IonLabel>
                <IonInput onIonChange={(e: any)=> {setNom(e.target.value)}} clearOnEdit={true} type="text"/>
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Prénom</IonLabel>
                <IonInput onIonChange={(e: any)=> {setPrenom(e.target.value)}} clearOnEdit={true} type="text" />
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Date de naisssance</IonLabel>
                <IonInput onIonChange={(e: any)=> {setDateNaissance(e.target.value)}} clearOnEdit={true} type="date"/>
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Email</IonLabel>
                <IonInput onIonChange={(e: any)=> {setEmail(e.target.value)}} clearOnEdit={true} type="text"/>
            </IonItem>
            <IonItem className='inputitem'>
                <IonLabel position="floating">Password</IonLabel>
                <IonInput onIonChange={(e: any)=> {setPassword(e.target.value)}} clearOnEdit={true} type="password"/>
            </IonItem>
            <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={save}>
                S'inscrire
            </IonButton>
        </form>
    )
}

export default Inscription