import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonMenu,IonButtons,IonMenuButton } from '@ionic/react';
import '../assets/myappcss/Form.css'

function Acceuil() {
    
    function nouveau() {
        window.location.replace("/enchere")
    }
    
    return (
       <>
            {/* <IonButton onClick={nouveau}>Nouveau Enchere</IonButton><br></br>
            <IonButton>Statistique Enchere</IonButton> */}
        <IonMenu contentId='main-content'>
        <IonHeader>
            <IonToolbar>
            <IonTitle>Menu</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent>
            <a id='lien' href='/enchere'>Nouveaux enchere</a><br></br><br></br>
            <a id='lien' href='/listeEnchere'>Liste enchere</a><br></br><br></br>
            <a id='lien' href="/recharge">Rechargement</a><br></br><br></br>
            <a id='lien' href="/">Deconexion</a><br></br><br></br>
        </IonContent>
        </IonMenu>
        <IonPage id='main-content'>
        <IonHeader>
            <IonToolbar>
            <IonButtons slot='start'>
                <IonMenuButton></IonMenuButton>
            </IonButtons>
            <IonTitle>MENU</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent className='ion-padding'>
            <h1 id='paragraphe'>Bienvenue</h1>
            <p id='paragraphe2'>dans le site de venteEnchere</p>
        </IonContent>
        </IonPage>
       </>
    )
}

export default Acceuil 