import { IonImg } from '@ionic/react';
import React, { useState, useEffect } from 'react';
import {Table, Container} from 'reactstrap';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonMenu,IonButtons,IonMenuButton } from '@ionic/react';

const ListeEnchere = () => {
   var [enchere, setencheres] = useState<any>([]);
   var [photos, setphotos] = useState<any>([]);
   var [etat, setEtat] = useState(true);
   var [etat2, setEtat2] = useState(true);
   function getData(){
       if(etat){
           fetch("http://encherebackend-production-21bf.up.railway.app/getEnchereAll/"+sessionStorage.getItem("iduser")).then((response) =>{
                   return response.json(); 
           }).then((liste)=>{
               setencheres(liste);
               setEtat(false)
           })
       }
   }
   useEffect(()=>{
       getData();
   },);


   function getImage(idenchere :any){
      if(etat2){
          fetch("http://encherebackend-production-21bf.up.railway.app/getPhotoParEnchere/"+idenchere).then((response) =>{
                  return response.json(); 
          }).then((liste)=>{
               setphotos(liste);
               setEtat2(false)
          })
      }
  }
  useEffect(()=>{
      getData();
  },);

   function deconnecter() {
      window.location.replace("/")
   }

   

    return(
       <>
        <IonMenu contentId='main-content'>
        <IonHeader>
            <IonToolbar>
            <IonTitle>Menu</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent>
            <a id='lien' href='/enchere'>Nouveaux enchere</a><br></br><br></br>
            <a id='lien' >Notifications</a><br></br><br></br>
            <a id='lien' href="/">deconexion</a><br></br><br></br>
        </IonContent>
        </IonMenu>
        <IonPage id='main-content'>
        <IonHeader>
            <IonToolbar>
            <IonButtons slot='start'>
                <IonMenuButton></IonMenuButton>
            </IonButtons>
            <IonTitle>MENU</IonTitle>
            </IonToolbar>
        </IonHeader>
        <IonContent className='ion-padding'>
        <div>
         <Container>
            <Table>
               <tbody>
               {enchere.map((enchere: any) => {
                  getImage(enchere.id)
                    return (
                        <tr>
                            <h1>{enchere.nom}</h1>
                            <h1>{enchere.id}</h1>
                            <p>{enchere.description}</p> 
                            <p>{enchere.date_enchere}</p> 
                            <p>{enchere.intitule}</p> 
                            
                            {photos.map((photos: any) => {
                              return (
                                    <IonImg src={photos.base64}></IonImg>
                              )
                           })
                           }
                           
                        </tr>
                    )
                })
                }
               </tbody>
            </Table>
         </Container>
      </div>
        </IonContent>
        </IonPage>
       </>
    );
};
export default ListeEnchere;