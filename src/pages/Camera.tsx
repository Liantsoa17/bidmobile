import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  IonIcon,
  IonGrid,
  IonRow,
  IonCol,
  IonImg,
  IonButton,
} 
from '@ionic/react';

import { camera, closeCircleOutline } from 'ionicons/icons';
import {  useState } from 'react';
import { supPhoto, usePhotoGallery } from '../hooks/usePhotoGallery';
// import { supAll, supPhoto, usePhotoGallery } from './hooks/usePhotoGallery';
const AjoutEnchere: React.FC = () => {
  const { photos, takePhoto, __photo } = usePhotoGallery();
  var [ph, setPh] = useState(__photo);
  ph = __photo;
  function supPh(id: String) {
      var __ph = supPhoto(id);
      setPh(__ph);
  }
  var url = ""
  function Ajouter() {
        
      for (let index = 0; index < __photo.length; index++) {
        let bodyimage = {
          "base64" : ''+__photo[index][0],
          "idenchere" : sessionStorage.getItem("idenchere")
        }
        url = "http://encherebackend-production-21bf.up.railway.app/InsertPhoto";
        fetch(url,{
          method: "POST",
          headers: {"Content-type": "application/json" },
          body: JSON.stringify(bodyimage)
        })
        console.log(bodyimage);
      }
      //window.location.replace("/acceuil")
  }
  return (
      <IonPage>
          <IonHeader>
              <IonToolbar>
                  <IonTitle>Photo Gallery</IonTitle>
              </IonToolbar>
          </IonHeader>
          <IonContent fullscreen>
              <IonGrid>
                  <IonRow>
                      {ph.map((p) => (
                          <IonCol size="6" >
                              <IonIcon icon={closeCircleOutline} onClick={() => supPh(p[1])}>{p[1]}</IonIcon>
                              <IonImg src={p[0] + ""} />
                          </IonCol>
                      ))}
                  </IonRow>
              </IonGrid>
              <IonContent>
                  <div className="ion-text-center">
                      Ajout photo(s)
                      <IonButton onClick={() => takePhoto()}>
                          <IonIcon icon={camera}></IonIcon>
                      </IonButton>
                  </div>
                  <IonButton onClick={Ajouter}>Valider</IonButton>
              </IonContent>
          </IonContent>
      </IonPage>
  );
};

export default AjoutEnchere;
