import { IonButton,  IonInput, IonItem, IonLabel } from '@ionic/react'
import React, { useState } from 'react'
import '../assets/myappcss/Form.css'


function Connexion() {
    var [email,setEmail] = useState("");
    var [mdp,setMdp] = useState("");
    function log() {
        fetch("http://localhost:8080/LoginUser/"+email+"&"+mdp).then((response) => {  
            return response.json();
        }).then((utilisateur)=>{
            if (utilisateur.id != 0) {
                sessionStorage.setItem("iduser",utilisateur.id)
                window.location.replace("/acceuil")
            }else{
                window.location.replace("")
            }
        });
    }
    return (
        <div className="ion-padding myform">
            <form>
                <IonLabel className='formtitle'>Se connecter</IonLabel>
                <IonItem className='inputitem'>
                    <IonLabel position="floating">Email</IonLabel>
                    <IonInput onIonChange={(e: any)=> {setEmail(e.target.value)}} clearOnEdit={true} type="text"/>
                </IonItem>
                <IonItem className='inputitem'>
                    <IonLabel position="floating">Password</IonLabel>
                    <IonInput onIonChange={(e: any)=> {setMdp(e.target.value)}} clearOnEdit={true} type="password"/>
                </IonItem>
                <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={log}>
                    Login
                </IonButton>
            </form>
            <a href='/inscription' id='inscription'>S'inscrire</a>
        </div>
        
    )
}

export default Connexion