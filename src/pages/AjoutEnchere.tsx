import { IonButton,  IonInput, IonItem, IonItemOption, IonLabel, IonSelect, IonSelectOption } from '@ionic/react'
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonMenu,IonList,IonButtons,IonMenuButton } from '@ionic/react';
import React, { useState,useEffect } from 'react'

import '../assets/myappcss/Form.css'

function AjoutEnchere() {
    var [nom,setNom] = useState("");
    var [description,setDescription] = useState("");
    var [prixMinimal,setPrixMinimal] = useState("");
    var [duree,setDuree] = useState("");
    var [idcategories,setidcategories] = useState("");
    var [message,setmessage] = useState("");
    var [categories, setcategories] = useState<any>([]);
    var [etat, setEtat] = useState(true);

    
    function enchere() {
        if(Number(duree) < 0 ){
            setmessage("duree invalid !!!!");
        }
        else if(Number(prixMinimal) < 0 ){
            setmessage("prix invalid !!!!");
        }
        else{
            fetch("http://encherebackend-production-21bf.up.railway.app/AjoutEnchere/"+nom+"&"+description+"&"+idcategories+"&"+prixMinimal+"&"+duree+"&"+sessionStorage.getItem("iduser")).then((response) => {  
                return response.json();
            }).then((enchere)=>{
                    sessionStorage.setItem("idenchere",enchere.id)
                    window.location.replace("/photo")
            });
        }
        
    }
        


    function getData(){
        if(etat){
            fetch("http://encherebackend-production-21bf.up.railway.app/getCategorieAll").then((response) =>{
                    return response.json(); 
            }).then((liste)=>{
                setcategories(liste);
                setEtat(false)
            })
        }
    }
    useEffect(()=>{
        getData();
    },);
    
    return (
        <div>
            <IonMenu contentId='main-content' >
                <IonHeader>
                <IonToolbar>
                <IonTitle>Menu</IonTitle>
                </IonToolbar>
                </IonHeader>
                <IonContent>
                    <a id='lien' href='/listeEnchere'>Liste enchere</a><br></br><br></br>
                    <a id='lien' href="/recharge">Rechargement</a><br></br><br></br>
                    <a id='lien' href="/">Deconexion</a><br></br><br></br>
                </IonContent>
            </IonMenu>
            <IonPage id='main-content'>
            <IonHeader>
                <IonToolbar>
                <IonButtons slot='start'>
                    <IonMenuButton></IonMenuButton>
                </IonButtons>
                <IonTitle>MENU</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent className='ion-padding'>
            <form className="ion-padding myform">
                <IonLabel className='formtitle'>Ajouter un Enchère</IonLabel>
                <IonItem className='inputitem'>
                    <IonLabel position="floating">Nom</IonLabel>
                    <IonInput onIonChange={(e: any)=> {setNom(e.target.value)}} clearOnEdit={true} type="text" />
                </IonItem>
                <IonItem className='inputitem'>
                    <IonLabel position="floating">Description</IonLabel>
                    <IonInput onIonChange={(e: any)=> {setDescription(e.target.value)}} clearOnEdit={true} type="text" />
                </IonItem>

                <IonItem className='inputitem'>
                    <IonLabel position="floating">Catégorie</IonLabel>
                    <IonSelect onIonChange={(e: any)=> {setidcategories(e.target.value)}} >
                        {categories.map((categories: any) => {
                             return (
                                    <IonSelectOption value={categories.id}>{categories.nom}</IonSelectOption>
                             )
                        })
                        }
                    </IonSelect>
                </IonItem>

                <IonItem className='inputitem'>
                    <IonLabel position="floating">Prix Minimal</IonLabel>
                    <IonInput onIonChange={(e: any)=> {setPrixMinimal(e.target.value)}} clearOnEdit={true} type="text"/>
                </IonItem>
                <IonItem className='inputitem'>
                    <IonLabel position="floating">Durée</IonLabel>
                    <IonInput onIonChange={(e: any)=> {setDuree(e.target.value)}} clearOnEdit={true} type="text"/>
                </IonItem>
                <IonButton className="ion-margin-top inputitem" type="button" expand="block" onClick={enchere}>
                    Ajouter
                </IonButton>
                <p>{message}</p>
            </form>	
            </IonContent>
            </IonPage>
        </div>
    )
}

export default AjoutEnchere