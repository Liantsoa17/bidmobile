import { Route } from 'react-router-dom';
import { IonApp,  IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './assets/myappcss/App.css';
import Inscription from './pages/Inscription';
import Acceuil from './pages/Acceuil';
import Header from './components/Header';
import Connexion from './pages/Connexion';
import AjoutEnchere from './pages/AjoutEnchere';
import ListeEnchere from './pages/ListeEnchere';
import Recharge from './pages/Recharge';
import Camera from './pages/Camera';


setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route exact path="/inscription">
          <Header/>
          <Inscription/>
        </Route>
        <Route exact path="">
          <Header/>
          <Connexion/>
        </Route>
        <Route exact path="/enchere">
          <Header/>
          <AjoutEnchere/>
        </Route>
        <Route exact path="/acceuil">
          <Header/>
          <Acceuil/>
        </Route>
        <Route exact path="/inscription">
          <Header/>
          <Inscription/>
        </Route>
        <Route exact path="/list">
          <Header/>
          <ListeEnchere/>
        </Route>
        <Route exact path="/recharge">
          <Header/>
          <Recharge/>
        </Route>
        <Route exact path="/photo">
          <Header/>
          <Camera></Camera>
        </Route>
        <Route exact path="/listeEnchere">
          <Header/>
          <ListeEnchere></ListeEnchere>
        </Route>
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
