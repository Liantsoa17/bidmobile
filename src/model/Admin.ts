export interface Admin {
    id: number;
    nom: string;
    mdp: string;
    email: string;
}