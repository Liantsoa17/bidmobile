export interface Enchere {
    id: number;
    nom: string;
    date_enchere: Date;
    description: string;
    idcategorie: number;
    prix_minimal: DoubleRange;
    duree: DoubleRange;
    idutilisateur: number;
    idstatus: number;
    commission: number;
}