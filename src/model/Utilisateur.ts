export interface Utilisateur {
    id: number;
    nom: string;
    prenom: string;
    mdp: string;
    email: string;
    date_naissance: Date;
}